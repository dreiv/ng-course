import { Injectable } from '@angular/core';

import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Recipe } from './recipe.model';

@Injectable()
export class RecipeService {
  private recipes: Recipe[] = [
    new Recipe(
      'Crepes',
      'A crepe is a very thin pancake',
      'assets/img/recipes/crepes.jpg',
      [
        new Ingredient('Flour', 2),
        new Ingredient('Salt', 1),
        new Ingredient('Milk', 5),
        new Ingredient('Eggs', 2)
      ]
    ),
    new Recipe(
      'Frech Toast',
      'French toast is a dish made of sliced bread soaked in eggs and milk, then fried.',
      'assets/img/recipes/frenchToast.jpg',
      [
        new Ingredient('Bread', 3),
        new Ingredient('Salt', 1),
        new Ingredient('Eggs', 2)
      ]
    )
  ];

  constructor(private slService: ShoppingListService) {}

  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(id: number) {
    return this.recipes[id];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.slService.addIngredients(ingredients);
  }
}
